import 'package:flutter/material.dart';

class BottomNavigation extends StatefulWidget {
  const BottomNavigation({Key? key}) : super(key: key);

  @override
  State<BottomNavigation> createState() => _BottomNavigationState();
}

class _BottomNavigationState extends State<BottomNavigation> {
  int selectedIndex = 0;

  void handleSelected(int index) {
    setState(() {
      selectedIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return BottomNavigationBar(
      type: BottomNavigationBarType.fixed,
      selectedFontSize: 12,
      unselectedFontSize: 12,
      showUnselectedLabels: false,
      selectedItemColor: Colors.indigo,
      currentIndex: selectedIndex,
      onTap: handleSelected,
      items: const <BottomNavigationBarItem>[
        BottomNavigationBarItem(
          icon: Icon(Icons.home),
          label: 'Home',
        ),
        BottomNavigationBarItem(
          icon: Icon(Icons.people_alt),
          label: 'Friends',
        ),
        BottomNavigationBarItem(
          icon: Icon(Icons.collections),
          label: 'Story',
        ),
        BottomNavigationBarItem(
          icon: CircleAvatar(
            radius: 13,
            backgroundImage: AssetImage('assets/images/avatar.jpg'),
          ),
          label: 'Profile',
        ),
      ],
    );
  }
}
