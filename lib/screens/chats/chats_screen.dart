import 'package:flutter/material.dart';
import 'package:lab4/screens/chats/components/body.dart';
import 'components/bottom_navigator.dart';

class ChatsScreen extends StatelessWidget {
  const ChatsScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: buildAppBar(),
      body: const Body(),
      bottomNavigationBar: const BottomNavigation(),
      floatingActionButton: FloatingActionButton(
        onPressed: () {},
        child: const Icon(Icons.person_add),
      ),
    );
  }

  AppBar buildAppBar() {
    return AppBar(
      elevation: 0,
      automaticallyImplyLeading: false,
      title: Row(children: const [
        SizedBox(
          // height: 32,
          child: CircleAvatar(
            radius: 14,
            backgroundImage: AssetImage('assets/images/avatar.jpg'),
          ),
        ),
        SizedBox(width: 12),
        Text('Chats')
      ]),
      actions: const [
        Padding(
          padding: EdgeInsets.all(8.0),
          child: Icon(Icons.camera),
        ),
        Padding(
          padding: EdgeInsets.all(8.0),
          child: Icon(Icons.edit),
        )
      ],
    );
  }
}
