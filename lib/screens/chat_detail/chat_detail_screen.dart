import 'package:flutter/material.dart';
import 'package:lab4/screens/chat_detail/components/body.dart';

import 'components/app_bar.dart';

class ChatDetailScreen extends StatefulWidget {
  ChatDetailScreen({Key? key}) : super(key: key);

  final List themeList = [
    Colors.indigo,
    Colors.blue,
    Colors.green,
    Colors.red,
    Colors.orange,
    Colors.purple,
    Colors.pink,
    Colors.yellow,
    Colors.teal,
    Colors.lime,
    Colors.cyan,
    Colors.lightBlue,
    Colors.lightGreen,
    Colors.amber,
    Colors.deepOrange,
  ];

  @override
  State<ChatDetailScreen> createState() => _ChatDetailScreenState();
}

class _ChatDetailScreenState extends State<ChatDetailScreen> {
  int themeIndex = 0;

  void handleChangeTheme() {
    setState(() {
      if (themeIndex == widget.themeList.length - 1) {
        themeIndex = 0;
        return;
      }
      themeIndex = themeIndex + 1;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Theme(
      data: ThemeData(primarySwatch: widget.themeList[themeIndex]),
      child: Scaffold(
        appBar: ChatDetailAppBar(
          handleChangeTheme: handleChangeTheme,
        ),
        body: const Body(),
      ),
    );
  }
}
