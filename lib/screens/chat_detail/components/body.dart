import 'package:flutter/material.dart';
import 'package:lab4/screens/chat_detail/components/message_display.dart';

import 'input_field.dart';

class Body extends StatelessWidget {
  const Body({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: const [
        MessageDisplay(),
        InputField(),
      ],
    );
  }
}
